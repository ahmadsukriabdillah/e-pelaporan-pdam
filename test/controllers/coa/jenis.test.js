process.env.NODE_ENV = 'test';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const app = require('./../../../app');
chai.use(chaiHttp);


describe('Jenis COA Test', () => {
    var token;
    before((done) => {
        chai.request(app)
            .post('/api/auth')
            .send({
                username: "admin",
                password: "admin"
            })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.data.profile.should.include.keys(
                    'cif', 'uid'
                );
                res.body.data.should.include.keys(
                    'token'
                );
                res.body.statusCode.should.eql(200);
                this.token = res.body.data.token;
                done();
            });
    });
    it('List Jenis COA', (done) => {
        chai.request(app)
            .get('/api/v1/coa')
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                done();
            });
    });
});