process.env.NODE_ENV = 'test';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const app = require('./../../../app');
chai.use(chaiHttp);
var jenis = 1;
var kelompok = 11;
var bukubesar = 1101;

describe(`Account COA Test`, () => {
    var token;
    var payload = {
        keterangan: "ACCOUNT TEST",
        golongan: 'USER'
    }
    var resp;
    before((done) => {
        chai.request(app)
            .post('/api/auth')
            .send({
                username: "admin",
                password: "admin"
            })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.data.profile.should.include.keys(
                    'cif', 'uid'
                );
                res.body.data.should.include.keys(
                    'token'
                );
                res.body.statusCode.should.eql(200);
                this.token = res.body.data.token;
                done();
            });
    });
    it(`Detail Account COA`, (done) => {
        chai.request(app)
            .get(`/api/v1/coa/${jenis}/kelompok/${kelompok}/bukubesar/${bukubesar}/account`)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                done();
            });
    });
    it('Create Account COA', (done) => {
        chai.request(app)
            .post(`/api/v1/coa/${jenis}/kelompok/${kelompok}/bukubesar/${bukubesar}/account`)
            .send(payload)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys

                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                this.resp = res.body.data;
                done();

            });
    });
    it("Detail Account COA", done => {
        chai.request(app)
            .get(`/api/v1/coa/${jenis}/kelompok/${kelompok}/bukubesar/${bukubesar}/account/${this.resp.acc}`)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                done();

            });
    });
    it("Update Account COA", done => {
        this.resp.keterangan = "UBAH";
        chai.request(app)
            .put(`/api/v1/coa/${jenis}/kelompok/${kelompok}/bukubesar/${bukubesar}/account/${this.resp.acc}`)
            .set('Authorization', this.token)
            .send(resp)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'data'
                );
                done();

            });
    });
    it("Delete Account COA", done => {
        chai.request(app)
            .delete(`/api/v1/coa/${jenis}/kelompok/${kelompok}/bukubesar/${bukubesar}/account/${this.resp.acc}`)
            .set('Authorization', this.token)
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode'
                );
                done();
            });
    })
});