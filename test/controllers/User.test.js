process.env.NODE_ENV = 'test';
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
const app = require('./../../app');
chai.use(chaiHttp);


describe('User API Test', () => {
    it('Login Success', (done) => {
        chai.request(app)
            .post('/api/auth')
            .send({
                username: "admin",
                password: "admin"
            })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(200);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(200);
                // the first object in the data array should
                // have the right keys
                res.body.data.profile.should.include.keys(
                    'cif', 'uid'
                );
                res.body.data.should.include.keys(
                    'token'
                );
                res.body.statusCode.should.eql(200);
                done();
            });
    });
    it('Failed Login', (done) => {
        chai.request(app)
            .post('/api/auth')
            .send({
                username: "admin",
                password: "sukri"
            })
            .end((err, res) => {
                res.status.should.equal(402);
                res.type.should.equal('application/json');
                res.body.should.include.keys(
                    'statusCode', 'publicMessage', 'developerMessage'
                );
                done();
            });
    });
    it('Login Validation Error', (done) => {
        chai.request(app)
            .post('/api/auth')
            .send({
                password: "sukri"
            })
            .end((err, res) => {
                // there should be no errors
                should.not.exist(err);
                // there should be a 200 status code
                res.status.should.equal(400);
                // the response should be JSON
                res.type.should.equal('application/json');
                // the JSON response body should have a
                // key-value pair of {"status": "success"}
                res.body.statusCode.should.eql(400);
                // the first object in the data array should
                // have the right keys
                res.body.should.include.keys(
                    'statusCode', 'publicMessage', 'developerMessage'
                );
                done();
            });
    });

});

