import "core-js/es6/promise";
import "core-js/es6/string";
import "core-js/es7/array";
import Vue from "vue";
import BootstrapVue from "bootstrap-vue";
import App from "./App";
import router from "./router";
import VeeValidate from "vee-validate";
import { VueAxios } from "./utils/request";
import VueStorage from "vue-ls";
import "./utils/filter";

const vee_config = {
  aria: true,
  classNames: {},
  classes: false,
  delay: 0,
  dictionary: null,
  errorBagName: "errors", // change if property conflicts
  events: "input|blur",
  fieldsBagName: "fields",
  inject: true,
  locale: "en",
  strict: true,
  validity: false
};

Vue.use(VueStorage, {
  namespace: "pro__", // key prefix
  name: "ls", // name variable Vue.[ls] or this.[$ls],
  storage: "local" // storage name session, local, memory
});
Vue.use(VueAxios);
Vue.use(BootstrapVue);
Vue.use(VeeValidate, vee_config);
/* eslint-disable no-new */
new Vue({
  el: "#app",
  router,
  template: "<App/>",
  components: {
    App
  }
});
