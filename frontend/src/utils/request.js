import Vue from "vue";
import axios from "axios";
import { VueAxios } from "./axios";
const ACCESS_TOKEN = "token";
// 创建 axios 实例
const service = axios.create({
  baseURL: "http://localhost:8000/",
  timeout: 6000
});

const err = error => {
  return Promise.reject(error);
};

// request interceptor
service.interceptors.request.use(config => {
  const token = Vue.ls.get(ACCESS_TOKEN);
  if (token) {
    config.headers["Authorization"] = token;
  }
  return config;
}, err);

// response interceptor
service.interceptors.response.use(response => {
  return response.data;
}, err);

const installer = {
  vm: {},
  install(Vue) {
    Vue.use(VueAxios, service);
  }
};

export { installer as VueAxios, service as axios };
