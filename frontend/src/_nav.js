export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
      badge: {
        variant: "primary",
        text: "NEW"
      }
    },
    {
      name: "Company Profile",
      url: "/profile",
      icon: "icon-speedometer"
    },
    {
      name: "Pengaduan",
      url: "/work",
      icon: "icon-speedometer"
    }
  ]
};
