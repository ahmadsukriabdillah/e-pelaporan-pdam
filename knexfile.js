// Update with your config settings.
module.exports = {
  development: {
    client: "pg",
    version: "7.10",
    connection: {
      host: "127.0.0.1",
      port: 5432,
      database: "e-pelaporan",
      user: "postgres",
      password: "1234"
    },
    pool: {
      min: 0,
      max: 10
    },
    debug: true
  },
  staging: {
    client: "pg",
    connection: {
      host: process.env.DB_URL,
      port: process.env.DB_PORT,
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASS
    },
    pool: {
      min: 2,
      max: 10
    },
    debug: true
  },
  test: {
    client: "pg",
    connection: {
      host: "127.0.0.1",
      database: "emikro-test",
      user: "postgres",
      password: "1234"
    },
    pool: {
      min: 2,
      max: 10
    }
  },
  production: {
    client: "pg",
    connection: {
      host: process.env.DB_URL,
      port: process.env.DB_PORT,
      database: process.env.DB_NAME,
      user: process.env.DB_USER,
      password: process.env.DB_PASS
    },
    pool: {
      min: 2,
      max: 10
    }
  }
};
