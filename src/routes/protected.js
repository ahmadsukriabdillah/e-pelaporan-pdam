const jwt = require('jsonwebtoken');
const {
    account,
    bukubesar,
    kelompok,
    jenis,
    anggota,
    users,
    masterDeposito,
    masterTabungan,
    masterPinjaman,
    jenisDeposito,
    jenisTabungan,
    jenisPinjaman
} = require('./../controllers');
const {
    UnauthorizerException,
} = require('./../base/exception');
const validate = require('express-validation');
const Joi = require('joi');
module.exports = (app) => {
    /**
     * @apiDefine DataNotFound
     *
     * @apiError DataNotFound The id of the Data was not found.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 404 Not Found
     *     {
     *       "statusCode": 404,
     *       "publicMessage": "Data You Search not found",
     *       "developerMessage": "Data You Search not found"   
     *     }
     */
    app.use((req, res, next) => {
        var token = req.body.token || req.query.token || req.headers['authorization'];
        if (token) {
            jwt.verify(token, app.get('secret'), function (err, decoded) {
                if (err) {
                    next(new UnauthorizerException("Unauthorization Request"));
                } else {
                    // if everything is good, save to request for use in other routes
                    if (decoded.st <= 0) {
                        next(new UnauthorizerException("User need to activeate"));
                    } else if (decoded.err_pass >= 3) {
                        next(new UnauthorizerException("User Has been suspended."));
                    } else {
                        req.decoded = decoded;
                        next();
                    }
                }
            });
        } else {
            next(new UnauthorizerException("Unauthorization Request."));

        }
    });
    /**
     * @api {get} /api/v1/coa Get List Jenis Account
     * @apiGroup Account Jenis
     * @apiName List Account Jenis
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/coa', jenis.findAll);
    /**
     * @api {get} /api/v1/coa Get Detail Jenis Account
     * @apiGroup Account Jenis
     * @apiName Detail Account Jenis
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId id jenis account
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/coa/:jenisId', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required()
        }
    }), jenis.findOne);
    /**
     * @api {post} /api/v1/coa Create Jenis Account
     * @apiGroup Account Jenis
     * @apiName Create Account Jenis
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Body Param) {String} accjenis id jenis account
     * @apiParam (Body Param) {String} jenis Nama jenis account
     *
     * @apiUse GlobalError
     */
    app.post('/api/v1/coa', validate({
        body: {
            accjenis: Joi.string().alphanum().max(2).required(),
            jenis: Joi.string().alphanum().max(30).required()
        }
    }), jenis.create);
    /**
     * @api {put} /api/v1/coa/:jenisId Update Jenis Account
     * @apiGroup Account Jenis
     * @apiName Update Account Jenis
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId id jenis account
     *
     * @apiParam (Body Param) {String} jenis Nama jenis account
     *
     * @apiUse GlobalError
     */
    app.put('/api/v1/coa/:jenisId', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required()
        },
        body: {
            jenis: Joi.string().alphanum().max(30).required()
        }
    }), jenis.update);
    /**
     * @api {delete} /api/v1/coa/:jenisId Delete Jenis Account
     * @apiGroup Account Jenis
     * @apiName Delete Account Jenis
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId id jenis account
     *
     *
     * @apiUse GlobalError
     */
    app.delete('/api/v1/coa/:jenisId', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required()
        }
    }), jenis.delete);

    /**
     * @api {get} /api/v1/coa/:jenisId/kelompok/ Get List Kelompok Account
     * @apiGroup Kelompok
     * @apiName List Kelompok
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiParam (Path Param) {int} jenisId Jenis Kelompok Account
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/coa/:jenisId/kelompok/', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required()
        }
    }), kelompok.findAll);
    /**
     * @api {get} /api/v1/coa/:jenisId/kelompok/:kelompokId Get Detail Kelompok Account
     * @apiGroup Kelompok
     * @apiName Detail Kelompok
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId Jenis Account
     * @apiParam (Path Param) {int} kelompokId Kelompok Account
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/coa/:jenisId/kelompok/:kelompokId', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required()
        }
    }), kelompok.findOne);
    /**
     * @api {post} /api/v1/coa/:jenisId/kelompok/ Create Kelompok Account
     * @apiGroup Kelompok
     * @apiName Create Kelompok Account
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId Jenis Account
     *
     * @apiParam (Body Param) {String} acckel Kelompok Account
     * @apiParam (Body Param) {String} Nama Kelompok Account
     * @apiParam (Body Param) {String} gol Kelompok Account
     * @apiParam (Body Param) {String} subgol Kelompok Account
     *
     * @apiUse GlobalError
     */
    app.post('/api/v1/coa/:jenisId/kelompok', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
        },
        body: {
            acckel: Joi.string().max(2).required(),
            kelompok: Joi.string().required(),
            gol: Joi.string().required(),
            subgol: Joi.string().required()
        }
    }), kelompok.create);
    /**
     * @api {put} /api/v1/coa/:jenisId/kelompok/:kelompokId Update Kelompok Account
     * @apiGroup Kelompok
     * @apiName Update Kelompok Account
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId Kelompok Account
     * @apiParam (Path Param) {int} kelompokId Kelompok Account
     *
     * @apiParam (Body Param) {String} Nama Kelompok Account
     * @apiParam (Body Param) {String} gol Kelompok Account
     * @apiParam (Body Param) {String} subgol Kelompok Account
     *
     * @apiUse GlobalError
     */
    app.put('/api/v1/coa/:jenisId/kelompok/:kelompokId', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required()
        },
        body: {
            kelompok: Joi.string().required(),
            gol: Joi.string().required(),
            subgol: Joi.string().required()
        }
    }), kelompok.update);
    /**
     * @api {delete} /api/v1/coa/:jenisId/kelompok/:kelompokId Delete Kelompok Account
     * @apiGroup Kelompok
     * @apiName Delete Kelompok Account
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {int} jenisId Kelompok Account
     * @apiParam (Path Param) {int} kelompokId Kelompok Account
     *
     *
     * @apiUse GlobalError
     */
    app.delete('/api/v1/coa/:jenisId/kelompok/:kelompokId', validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required()
        }
    }), kelompok.delete);


    /**
     * @api {get} /api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar Get List Bukubesar Account
     * @apiGroup Bukubesar
     * @apiName List Bukubesar
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiParam (Path Param) {int} jenisId Jenis Account
     * @apiParam (Path Param) {int} kelompokId Kelompok Account
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar',validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required()
        }
    }), bukubesar.findAll);
    /**
     * @api {get} /api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar Get Detail Bukubesar
     * @apiGroup Bukubesar
     * @apiName Detail Bukubesar
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     * 
     * @apiParam (Path Param) {String} jenisId Jenis Account
     * @apiParam (Path Param) {String} kelompokId Kelompok Account
     * @apiParam (Path Param) {String} bukubesarId Bukubesar Account
     * 
     */
    app.get('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId',validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required()
        }
    }), bukubesar.findOne);
    /**
     * 
     * @api {post} /api/v1/coa/jenisId/:kelompokId/bukubesar Create Bukubesar
     * @apiName Create Bukubesar
     * @apiGroup Bukubesar
     * @apiVersion  1.0.0
     * 
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     * 
     * @apiParamExample  {json} Request-Example:
     * {
     *     bukubesar : "BUKUBESAR",
     *     kategori  : "GL"
     *     golongan  : "USER"
     * }
     * 
     * 
     * @apiSuccessExample {type} Success-Response:
     *      HTTP/1.1 200 OK:
     *      {
     *        statusCode : 200
     *        data       : {
     *          bukubesar : "BUKUBESAR",
     *          kategori  : "GL"
     *          golongan  : "USER"
     *        }
     *      }
     * @apiUse GlobalError
     * 
     */
    app.post('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar',
    validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required()
        }
    }), bukubesar.create);
    /**
     * 
     * @api {put} /api/v1/coa/jenisId/:kelompokId/bukubesar/:bukubesarId Update Bukubesar
     * @apiName Update Bukubesar
     * @apiGroup Bukubesar
     * @apiVersion  1.0.0
     * 
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     * 
     * @apiParamExample  {json} Request-Example:
     * {
     *     bukubesar : "BUKUBESAR",
     *     kategori  : "GL"
     *     golongan  : "USER"
     * }
     * 
     * 
     * @apiSuccessExample {type} Success-Response:
     * HTTP/1.1 200 OK:
     * {
     *     statusCode : 200
     *     data       : {
     *      bukubesar : "BUKUBESAR",
     *      kategori  : "GL"
     *      golongan  : "USER"
     *     }
     * }
     * @apiUse GlobalError
     * 
     */
    app.put('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId',
    validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required(),
            bukubesarId: Joi.string().alphanum().max(5).required()
        }
    }), bukubesar.update);
    /**
     * 
     * @api {delete} /api/v1/coa/jenisId/:kelompokId/bukubesar/:bukubesarId Delete Bukubesar
     * @apiName Delete Bukubesar
     * @apiGroup Bukubesar
     * @apiVersion  1.0.0
     * 
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     * 
     * 
     * 
     * @apiSuccessExample {type} Success-Response:
     * HTTP/1.1 200 OK:
     * {
     *     statusCode : 200
     *     data       : {
     *      bukubesar : "BUKUBESAR",
     *      kategori  : "GL"
     *      golongan  : "USER"
     *     }
     * }
     * @apiUse GlobalError
     * 
     */
    app.delete('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId',
    validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required(),
            bukubesarId: Joi.string().alphanum().max(5).required()
        }
    }),bukubesar.delete);


    /**
     * @api {post} /api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId/account Get List Sub Bukubesar Account
     * @apiGroup Sub Bukubesar
     * @apiName List Sub Bukubesar
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiParam (Path Param) {int} jenisId Jenis Account
     * @apiParam (Path Param) {int} kelompokId Kelompok Account
     * @apiParam (Path Param) {int} bukubesarId Bukubesar Account
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId/account',
        validate({
            params: {
                jenisId: Joi.string().alphanum().max(2).required(),
                kelompokId: Joi.string().alphanum().max(3).required(),
                bukubesarId: Joi.string().alphanum().max(5).required()
            }
        }), account.findAll);
    app.get('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId/account/:accountId',
        validate({
            params: {
                jenisId: Joi.string().alphanum().max(2).required(),
                kelompokId: Joi.string().alphanum().max(3).required(),
                bukubesarId: Joi.string().alphanum().max(5).required(),
                accountId: Joi.string().alphanum().max(7).required()
            }
        }), account.findOne);
    app.post('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId/account',
    validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required(),
            bukubesarId: Joi.string().alphanum().max(5).required()
        }
    }), account.create);
    app.put('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId/account/:accountId',validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required(),
            bukubesarId: Joi.string().alphanum().max(5).required(),
            accountId: Joi.string().alphanum().max(7).required(),
        }
    }), account.update);
    app.delete('/api/v1/coa/:jenisId/kelompok/:kelompokId/bukubesar/:bukubesarId/account/:accountId',validate({
        params: {
            jenisId: Joi.string().alphanum().max(2).required(),
            kelompokId: Joi.string().alphanum().max(3).required(),
            bukubesarId: Joi.string().alphanum().max(5).required(),
            accountId: Joi.string().alphanum().max(7).required(),
        }
    }), account.delete);

    /**
     * @api {get} /api/v1/anggota Get List Anggota
     * @apiGroup Anggota
     * @apiName Get List Anggota
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/anggota', anggota.findAll);
    /**
     * @api {get} /api/v1/anggota/:anggotaId Get Detail Anggota
     * @apiGroup Anggota
     * @apiName Get Detail Anggota
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {String} anggotaId Anggota ID
     * 
     * @apiSuccessExample {json} Success-Response:
     * HTTP/1.1 200 OK:
     *    {
    "statusCode": 200,
    "data": {
        "model": [
            {
                "cib": "00001",
                "nokartu": "-",
                "nip": "",
                "nogaji": "-",
                "nama": "Mulyati",
                "ki": "01",
                "panggilan": "-",
                "kl": "P",
                "tmp_lahir": "-",
                "tgl_lahir": "0999-12-31T17:00:00.000Z",
                "id": "KTP",
                "no_id": "-",
                "tglexpired": "0999-12-31T17:00:00.000Z",
                "agama": "ISLAM",
                "pendidikan": "STRATA 1 (S-1)",
                "pekerjaan": "PEGAWAI NEGRI SIPIL (PNS)",
                "jabatan": "STAF",
                "golongan": "-",
                "alamat": "-",
                "telepon": "-",
                "handphone": "-",
                "e_mail": "-",
                "namaahliwaris": "-",
                "hubkel": "-",
                "alamatahliwaris": "-",
                "tglbuka": null,
                "st": 2,
                "wajib": 0,
                "pokok": 0,
                "tambah_voucher": 0,
                "pakai_voucher": 0,
                "saldo_voucher": 0,
                "tambah_plafond": 0,
                "pakai_plafond": 0,
                "saldo_plafond": 0,
                "tambah_coin": 0,
                "pakai_coin": 0,
                "saldo_coin": 0,
                "total_belanja": 0,
                "cif": "00040"
            }
        ],
        "pagination": {
            "page": 1,
            "total_page": 182,
            "filtered_record": "1",
            "total_record": 182,
            "order_by": "cib",
            "sort_by": "ASC"
        }
    }
}
     */
    app.get('/api/v1/anggota/:cibId/',validate({
        params: {
            cibId: Joi.string().alphanum().max(5).required()
        }
    }), anggota.findOne);
    app.post('/api/v1/anggota', anggota.create);
    app.put('/api/v1/anggota/:cibId',validate({
        params: {
            cibId: Joi.string().alphanum().max(5).required()
        }
    }), anggota.update);
    /**
     * 
     * @api {delete} /api/v1/anggota/:cibId Delete Anggota
     * @apiName Delete Anggota
     * @apiGroup Anggota
     * @apiVersion  1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     * 
     * @apiParam (Path Param) {String} cibId Nomor Anggota
     * 
     */
    app.delete('/api/v1/anggota/:cibId',validate({
        params: {
            cibId: Joi.string().alphanum().max(5).required()
        }
    }), anggota.delete);

    /**
     * @api {post} /api/v1/users/change_password Change Password
     * @apiGroup Users
     * @apiName Change Password
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     * @ApiParam {String} oldPassword Old User Password
     * @ApiParam {String} newPassword New User Password
     * @ApiParam {String} confirmPassword Confirm New User Password
     * @apiParamExample  {json} Request-Example:
     *    {
     *       "oldpassword": "oldpass",
     *       "newPassword": "newPass",
     *       "confirmPassword" : "new"
     *    }
     * @apiUse GlobalError
     */
    app.post('/api/v1/users/change_password',validate({
        body:{
            oldPassword: Joi.string().alphanum().required(),
            newPassword: Joi.string().alphanum().required(),
            confirmPassword: Joi.string().alphanum().required(),
        }
    }), users.changePassword);

    /**
     * @api {get} /api/v1/depositos Get List Jenis Deposito
     * @apiGroup Deposito
     * @apiName Get List Jenis Deposito
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/depositos/', jenisDeposito.findAll);
    /**
     * @api {get} /api/v1/depositos/:jenis Get Detail Jenis Deposito
     * @apiGroup Deposito
     * @apiName Get Detail Jenis Deposito
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {String} [jenis] Jenis Id Deposito
     * 
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/depositos/:jenis', jenisDeposito.findOne);
    /**
     * @api {get} /api/v1/depositos/:jenis/master Get List Master Deposito
     * @apiGroup Deposito
     * @apiName Get List Master Deposito
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/depositos/:jenis/master', masterDeposito.findAll);
     /**
     * @api {get} /api/v1/depositos/:jenis/master/:master Get Detail Master Deposito
     * @apiGroup Deposito
     * @apiName Get Detail Master Deposito
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Path Param) {String} [jenis] Jenis Deposito
     * @apiParam (Path Param) {String} [master] No. Master Deposito
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/depositos/:jenis/master/:master', masterDeposito.findOne);
    /**
     * @api {get} /api/v1/pinjaman/ Get List Jenis Pinjaman
     * @apiGroup Pinjaman
     * @apiName Get List Jenis Pinjaman
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/pinjaman/', jenisPinjaman.findAll);
    app.get('/api/v1/pinjaman/:jenis', jenisPinjaman.findOne);
    /**
     * @api {get} /api/v1/pinjaman/:jenis/master Get List Master Pinjaman
     * @apiGroup Pinjaman
     * @apiName Get List Master Pinjaman
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/pinjaman/:jenis/master', masterPinjaman.findAll);
    app.get('/api/v1/pinjaman/:jenis/master/:master', masterPinjaman.findOne);

    /**
     * @api {get} /api/v1/tabungan/ Get List Jenis Tabungan
     * @apiGroup Tabungan
     * @apiName Get List Jenis Tabungan
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/tabungan/', jenisTabungan.findAll);
    app.get('/api/v1/tabungan/:jenis', jenisTabungan.findOne);
    /**
     * @api {get} /api/v1/tabungan/:jenis/master Get List Master Tabungan
     * @apiGroup Tabungan
     * @apiName Get List Master Tabungan
     * @apiVersion 1.0.0
     * @apiHeader {String} Authorization authorization token from Login
     * @apiHeader {String} Content-Type content type default "application/json"
     *
     * @apiParam (Query Param) {int} [limit=100] limit data fetch form database
     * @apiParam (Query Param) {String} [sort_by=cib] limit data fetch form database
     * @apiParam (Query Param) {String} [oreder_by=asc] limit data fetch form database
     * @apiParam (Query Param) {int} [page=1] limit data fetch form database
     * @apiParam (Query Param) {boolean} [paging=true] enable or disable pagging feature
     *
     * @apiUse GlobalError
     */
    app.get('/api/v1/tabungan/:jenis/master', masterTabungan.findAll);
    app.get('/api/v1/tabungan/:jenis/master/:master', masterTabungan.findOne);
};