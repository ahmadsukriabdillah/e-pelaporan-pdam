// const users = require("../controllers").users;
// const database = require('./../utils/db');
// const {SystemError} = require('./../base/exception');
const validate = require("express-validation");
const Joi = require("joi");

const { reportController, userController } = require("./../controllers");

module.exports = app => {
  app.get(
    "/api/report",
    validate({
      query: {}
    }),
    reportController.findAll
  );
  app.get(
    "/api/report/:ticket_id",
    validate({
      query: {}
    }),
    reportController.findOne
  );

  app.post(
    "/api/auth",
    validate({
      body: {
        email: Joi.string()
          .email()
          .max(50),
        username: Joi.string()
          .alphanum()
          .max(50),
        password: Joi.string()
          .alphanum()
          .max(50)
          .required()
      }
    }),
    userController.auth
  );
};
