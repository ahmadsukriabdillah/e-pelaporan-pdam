const reportController = require("./report-controller");
const userController = require('./users-controller');
module.exports = {
  reportController,
  userController
};
