const database = require('./../../utils/db');
const {
    ValidationException,
    SystemError,
    FailedTaskExecution
} = require('./../../base/exception');
const {
    pagging
} = require('./../../utils/pagging');
var table_name = "account";
var view_name = "account";
module.exports = {
    findAll(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .where("cif", cif)
            .andWhere("accbb", req.params.bukubesarId);
        let params = req.query;
        params.order_by = params.order_by ? params.order_by : "acc";
        params.sort_by = params.sort_by ? params.sort_by : "ASC";
        pagging(sql, params).then(result => {
            sql
                .clearSelect()
                .select()
                .then((data) => {
                    res.status(200).json({
                        status_code: 200,
                        data: {
                            model: data,
                            pagination: result.page
                        }
                    });
                })
                .catch((err) => {
                    next(new FailedTaskExecution("Action Can't processed.", err.message));
                });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });

    },
    findOne(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("acc", req.params.accountId);
        sql
            .first()
            .then((result) => {
                if (result == undefined) {
                    return next(new FailedTaskExecution("Action Can't processed.", "Data Tidak di temukan"));
                } else {
                    res.status(200).json({
                        status_code: 200,
                        data: result
                    });
                }
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            accbb: req.params.bukubesarId,
            acc: req.body.acc || database.raw(`(SELECT max(acc)::integer + 1 from ${table_name} where accbb = ? and cif = ?)`, [req.params.bukubesarId, cif]),
            keterangan: req.body.keterangan,
            golongan: req.body.golongan || 'USER',
            cif: cif
        };
        let sql = database
            .insert(payload)
            .returning(["accbb", "acc", "keterangan", "golongan"])
            .into(table_name)
            .toQuery()
        database.raw(sql).then(resp => {
            res.status(200).json({
                status_code: 200,
                data: resp.rows[0]
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    update(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("acc", req.params.accountId);
        sql
            .first()
            .then((result) => {
                if (result == undefined) {
                    return next(new FailedTaskExecution("Action Can't processed.", "Data Tidak di temukan"));
                } else {
                    let payload = {
                        keterangan: req.body.keterangan || result.keterangan,
                        golongan: req.body.golongan || result.golongan,
                    };
                    database(table_name)
                        .where("cif", cif)
                        .returning(["accbb", "acc", "keterangan", "golongan"])
                        .andWhere("acc", req.params.accountId)
                        .update(payload).then(resp => {
                            res.status(200).json({
                                status_code: 200,
                                data: resp[0]                            });
                        }).catch(err => {
                            next(new FailedTaskExecution("Action Can't processed.", err.message));
                        });
                }
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });

        
    },
    delete(req, res, next) {
        const {
            cif
        } = req.decoded;

        database(table_name)
            .where("cif", cif)
            .andWhere("acc", req.params.accountId)
            .delete().then(() => {
                res.status(200).json({
                    status_code: 200,
                    message: "Succesfull"
                });
            }).catch(err => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    }
};