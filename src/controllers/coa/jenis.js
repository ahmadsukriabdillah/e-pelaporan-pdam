const database = require('./../../utils/db');;
const {
    ValidationException,
    SystemError,
    FailedTaskExecution
} = require('./../../base/exception');
const {
    pagging
} = require('./../../utils/pagging');
var table_name = "account_jenis";
var view_name = "account_jenis";
module.exports = {
    findAll(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif);
        let params = req.query;
        params.order_by = params.order_by ? params.order_by : "accjenis";
        params.sort_by = params.sort_by ? params.sort_by : "ASC";
        pagging(sql, params).then(result => {
            sql
                .clearSelect()
                .select()
                .then((data) => {
                    res.status(200).json({
                        statusCode: 200,
                        data: {
                            model: data,
                            pagination: result.page
                        }
                    });
                })
                .catch((err) => {
                    next(new FailedTaskExecution("Action Can't processed.", err.message));
                });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    findOne(req, res, next) {
        const {
            cif
        } = req.decoded;
        let sql = database
            .from(view_name)
            .select()
            .where("cif", cif)
            .andWhere("accjenis", req.params.jenisId);
        sql
            .first()
            .then((result) => {
                res.status(200).json({
                    statusCode: 200,
                    data: result
                });
            })
            .catch((err) => {
                next(new FailedTaskExecution("Action Can't processed.", err.message));
            });
    },
    create(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            accjenis: req.body.accjenis,
            jenis: req.body.jenis,
            cif: cif
        };
        database(table_name)
            .insert(payload).then(() => {
            res.status(200).json({
                statusCode: 200,
                data: payload
            });
        }).catch(err => {
            next(err);
        });
    },
    update(req, res, next) {
        const {
            cif
        } = req.decoded;

        let payload = {
            jenis: req.body.jenis,
        };
        database(table_name)
            .where("cif", cif)
            .andWhere("accjenis", req.params.jenisId)
            .update(payload).then(() => {
            res.status(200).json({
                statusCode: 200,
                data: payload
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    },
    delete(req, res, next) {
        const {
            cif
        } = req.decoded;

        database(table_name)
            .where("cif", cif)
            .andWhere("accjenis", req.params.jenisId)
            .delete().then(() => {
            res.status(200).json({
                statusCode: 200,
                publicMessage: "Succesfull"
            });
        }).catch(err => {
            next(new FailedTaskExecution("Action Can't processed.", err.message));
        });
    }
};