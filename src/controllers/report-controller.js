const { FailedTaskExecution } = require("./../base/exception");
const { toResponse } = require("./../base/base.response");
const { reportService } = require("./../services");
module.exports = {
  async findAll(req, res, next) {
    try {
      let data = await reportService.findAll(req);
      toResponse(req, res, data);
    } catch (err) {
      next(new FailedTaskExecution("Action Can't processed.", err.message));
    }
  },
  async findOne(req, res, next) {
    try {
      let data = await reportService.findOne(req);
      toResponse(req, res, data);
    } catch (err) {
      next(new FailedTaskExecution("Action Can't processed.", err.message));
    }
  }
};
