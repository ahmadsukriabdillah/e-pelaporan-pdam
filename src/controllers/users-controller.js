const { userService } = require('./../services');
const {
  ValidationException,
  UnauthorizerException,
  SystemError,
  FailedTaskExecution
} = require("../base/exception");
const { toResponse } = require("./../base/base.response");

module.exports = {
  async auth(req, res, next) {
    try {
      let data = await userService.authentication(req);
      toResponse(req, res, data);
    } catch (err) {
      next(new FailedTaskExecution("Action Can't processed.", err.message));
    }
  },
};
