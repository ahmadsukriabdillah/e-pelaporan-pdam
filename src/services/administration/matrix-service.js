const { db, logger } = require("./../../utils");

const findAll = async ({ query: { limit, page, sort_by, order_by } }) => {
  let data = await db.from("m_matrix")
    .where({parent_id: 0});

  return {
    status_code: 200,
    data: data
  };
};
const findOne = async ({ params: { matrix_id } }) => {
  let matrix = await db
    .from("m_matrix")
    .where({ id: matrix_id })
    .first();  
  return {
    status_code: 200,
    data: matrix
  };
};

const create = async ({body: { desc, parent_id, is_cwc, sla_limit, sla_type }}) => {
    let create = await db.from("m_matrix").insert({
        desc: desc,
        parent_id: parent_id,
        is_cwc: is_cwc,
        sla_limit: sla_limit,
        sla_type: sla_type,
        tenant_id: 'SC_PDAM'
    });
}
const update = async ({params: { matrix_id }, body: { desc, parent_id, is_cwc, sla_limit, sla_type } }) => {
    let create = await db.from("m_matrix")
    .where({
        id: matrix_id
    })
    .update({
        desc: desc,
        parent_id: parent_id,
        is_cwc: is_cwc,
        sla_limit: sla_limit,
        sla_type: sla_type,
        tenant_id: 'SC_PDAM'
    });
}

module.exports = {
  findAll,
  findOne,
  create
};
