const { db, logger, config } = require("./../../utils");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const authentication = async ({ body: { username, email, password } }) => {
  let user = await db
    .from("m_login")
    .where({ username: username || '' })
    .orWhere({email: email || ''})
    .first();

  const match = await bcrypt.compare(password, user.password || '');
  if (match) {
    delete user.password;
    let token = jwt.sign(user, config.secret, {
      expiresIn: "3h"
    });
    return {
      status_code: 200,
      data: {
        token: token,
        user: user
      }
    };
  } else {
    return {
      status_code: 401,
      message: "Username / password tidak benar.",
      developer_message: "Username / password tidak benar."
    };
  }
};

module.exports = {
  authentication
};
