const { db, limitless, logger } = require("./../utils");

const findAll = async ({ query: { limit, page, sort_by, order_by, status } }) => {
  let sql = db.from("contact");
  if(status){
    sql.where({
      status: status
    });
  }
  let sql_coumt = db.from("contact");

  if(status){
    sql.where({
      status: status
    });
    sql_coumt.where({
      status: status
    });
  }
  let { data, page_info } = await limitless(sql, sql_coumt, {
    limit: limit,
    page_number: page,
    sort_by: sort_by || "ticket_id",
    order_by: order_by || "DESC"
  });
  return {
    status_code: 200,
    data: {
      models: data,
      page_info: page_info
    }
  };
};
const findOne = async ({ params: { ticket_id } }) => {
  let ticket = await db
    .from("contact")
    .where({ ticket_id: ticket_id })
    .first();
  let { nomor_laporan } = ticket;
  let [ticket_log, laporan, images, log, evidance] = await Promise.all([
    db.from("contact_log").where({ ticket_id: ticket_id }),
    db
      .from("m_laporan")
      .where({ nomor_laporan: nomor_laporan })
      .first(),
    db
      .select("img_laporan")
      .from("m_laporan_image")
      .where({ nomor_laporan: nomor_laporan })
      .map(data => data.img_laporan),
    db.from("m_laporan_log").where({ nomor_laporan: nomor_laporan }),
    db.from("m_laporan_evidence").where({ nomor_laporan: nomor_laporan })
  ]);
  return {
    status_code: 200,
    data: {
      ...ticket,
      ticket_log,
      laporan,
      images,
      log,
      evidance
    }
  };
};

module.exports = {
  findAll,
  findOne
};
