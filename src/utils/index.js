const { db } = require("./db");
const { logger } = require("./logger");
const { pagging, limitless } = require("./pagging");
const config = require('./config');
module.exports = {
  db,
  pagging,
  logger,
  limitless,
  config
};
