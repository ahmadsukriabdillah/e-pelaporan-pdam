const { db } = require("./db");

const pagging = (sql, params) => {
  var limit = params.limit || 100;
  var page = params.page || 1;
  var count = sql;

  if (params.paging == undefined || params.paging == "true") {
    return new Promise((resolve, reject) => {
      count
        .clearSelect()
        .count("*")
        .then(res => {
          sql
            .offset(parseInt(limit) * parseInt(page) - parseInt(limit))
            .limit(parseInt(limit) || 100)
            .orderBy(params.order_by, params.sort_by);
          resolve({
            sql: sql,
            page: {
              page: page,
              total_page:
                parseInt(res[0].count) >= limit
                  ? Math.round(parseInt(res[0].count) / limit, 10)
                  : 1,
              filtered_record:
                parseInt(res[0].count) >= limit
                  ? limit
                  : parseInt(res[0].count),
              total_record: parseInt(res[0].count),
              order_by: params.order_by,
              sort_by: params.sort_by
            }
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  } else {
    return new Promise((resolve, reject) => {
      count
        .clearSelect()
        .count("*")
        .then(res => {
          sql.orderBy(params.order_by, params.sort_by);
          resolve({
            sql: sql,
            page: {
              page: 1,
              total_page: 1,
              filtered_record: parseInt(res[0].count),
              total_record: parseInt(res[0].count),
              order_by: params.order_by,
              sort_by: params.sort_by
            }
          });
        })
        .catch(err => {
          reject(err);
        });
    });
  }
};

const limitless = async (
  sql,
  sql_count,
  { limit = 100, page_number = 1, order_by, sort_by }
) => {
  const [result, count] = await Promise.all([
    new Promise((resolve, reject) => {
      sql
        .offset(parseInt(limit) * parseInt(page_number) - parseInt(limit))
        .limit(parseInt(limit))
        .orderBy(sort_by, order_by)
        .then(result => {
          resolve(result);
        })
        .catch(err => {
          reject(err);
        });
    }),
    new Promise((resolve, reject) => {
      sql_count
        .clearSelect()
        .clearOrder()
        .count(sort_by)
        .then(res => {
          resolve({
            page: page_number,
            total_page: Math.ceil(parseInt(res[0].count) / limit),
            filtered_record:
              limit < parseInt(res[0].count) ? limit : parseInt(res[0].count),
            total_record: parseInt(res[0].count),
            order_by: order_by,
            sort_by: sort_by
          });
        })
        .catch(err => {
          reject(err);
        });
    })
  ]);
  return {
    data: result,
    page_info: count
  };
};

module.exports = {
  pagging,
  limitless
};
