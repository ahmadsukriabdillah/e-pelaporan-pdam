let Response = () => {
  return {
    status_code: 200,
    message: "Succesfull",
    developer_message: "Succesfull"
  }
};
const toResponse = (req, res, payload) => {
  const elapsedHrTime = process.hrtime(req.start_time);
  const elapsedTimeInMs = elapsedHrTime[0] * 1000 + elapsedHrTime[1] / 1e6;
  const memory_usage = process.memoryUsage().heapUsed / 1024 / 1024;
  res
    .status(payload.status_code)
    .json(
      Object.assign(Response(), {
        ...payload,
        anality: {
          status: payload.status || 200,
          elapse_time: elapsedTimeInMs,
          memory_usage: `${Math.round(memory_usage * 100) / 100} MB`
        }
      })
    )
    .end();
};

module.exports = {
  Response,
  toResponse
};
