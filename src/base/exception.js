class BaseException extends Error {
  constructor(...args) {
    super(...args);
    this.status_code = 200;
    this.message = "Succesfull";
    this.developer_message = "Succesfull";
  }

  setstatus_code(status) {
    this.message = status;
    return this;
  }

  setmessage(message) {
    this.message = message;
    return this;
  }

  setdeveloper_message(message) {
    this.developer_message = message;
    return this;
  }

  setValidationError(err) {
    this.validation_error = err;
    return this;
  }

  toJson() {
    return {
      status_code: this.status_code,
      message: this.message,
      developer_message: this.developer_message,
      validation_error: this.validation_error
    };
  }
}

class FailedTaskExecution extends BaseException {
  constructor(...args) {
    super(...args);
    this.status_code = 503;
    if (args[0] != undefined) {
      this.message = args[0];
    } else {
      this.message = "Operation failed to execute.";
    }
    if (args[1] != undefined) {
      this.developer_message = args[1];
    } else {
      this.developer_message = "Operation failed to execute.";
    }
    Error.captureStackTrace(this, FailedTaskExecution);
  }
}

class NotFoundException extends BaseException {
  constructor(...args) {
    super(...args);
    this.status_code = 404;
    if (args[0] != undefined) {
      this.message = args[0];
    } else {
      this.message = "Data Not Found.";
    }
    if (args[1] != undefined) {
      this.developer_message = args[1];
    } else {
      this.developer_message = "Data Not Found Check Validity you input.";
    }
    Error.captureStackTrace(this, NotFoundException);
  }
}
class UserNeedActivation extends BaseException {
  constructor(...args) {
    super(...args);
    this.status_code = 402;
    if (args[0] != undefined) {
      this.message = args[0];
    } else {
      this.message = "User Tidak aktif, aktifkan terlebih dahulu.";
    }
    if (args[1] != undefined) {
      this.developer_message = args[1];
    } else {
      this.developer_message = "st = 0";
    }
    Error.captureStackTrace(this, UserNeedActivation);
  }
}

class UnauthorizerException extends BaseException {
  constructor(...args) {
    super(...args);
    this.status_code = 401;
    if (args[0] != undefined) {
      this.message = args[0];
    } else {
      this.message = "Unauthorization Request.";
    }
    if (args[1] != undefined) {
      this.developer_message = args[1];
    } else {
      this.developer_message = "Invalid Credential.";
    }
    Error.captureStackTrace(this, UnauthorizerException);
  }
}

class ValidationException extends BaseException {
  constructor(...args) {
    super(...args);
    this.status_code = 400;
    this.developer_message = "wrong format request.";
    this.message = "missing mandatory data.";
    Error.captureStackTrace(this, ValidationException);
  }
}

class SystemError extends BaseException {
  constructor(...args) {
    super(...args);
    this.status_code = 500;
    this.developer_message = args[0];
    this.message = "server unavailable.";
    this.developer_message = this.stack;
    Error.captureStackTrace(this, SystemError);
  }
}

const ExceptionMapper = [
  UnauthorizerException,
  ValidationException,
  SystemError,
  FailedTaskExecution,
  UserNeedActivation,
  NotFoundException
];

module.exports = {
  UnauthorizerException,
  ValidationException,
  ExceptionMapper,
  SystemError,
  FailedTaskExecution,
  UserNeedActivation,
  NotFoundException
};
