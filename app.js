/**
 * INCLUDE LIBRARY
 */

const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const config = require("./config"); // get our config file
const app = express();
const cors = require("cors");
const helmet = require("helmet");
const ev = require("express-validation");
const compression = require("compression");

const { ExceptionMapper, SystemError } = require("./src/base/exception");
const { Response } = require("./src/base/base.response");

/**
 *
 * KONFIGURASI
 */
const isProduction = process.env.NODE_ENV !== "production";

// app.use(expressValidator());
app.use((req, res, next) => {
  req.start_time = process.hrtime();
  next();
});
app.use(compression());
app.use(cors());
// app.use(morgan('combined', {
//     stream: logger.stream
// }));
app.use(
  morgan(":method :url :status :res[content-length] - :response-time ms")
);
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(helmet());
app.set("secret", config.secret);
app.disable("x-powered-by");
// app.use(express.static(path.join(__dirname, 'public/apidoc/')));
// app.get('/apidoc/*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'public/apidoc/index.html'));
// });
require("./src/routes/public")(app);
// require('./src/routes/protected')(app);
app.use((err, req, res, next) => {
  if (err instanceof ev.ValidationError) {
    return res.status(err.status || 400).json(
      Object.assign(Response(), {
        status_code: err.status,
        message: "Missing required parameter",
        developer_message: err.message,
        validation_error: err.errors.map(data => {
          return {
            field: data.field,
            location: data.location,
            message: data.messages[0]
          };
        })
      })
    );
  }
  if (!isProduction) {
    let send = false;
    ExceptionMapper.forEach(element => {
      if (err instanceof element) {
        send = true;
        return res.status(err.status_code).json(err.toJson());
      }
    });
    if (!send) {
      let sys_err = new SystemError();
      return res.status(sys_err.status_code).json(sys_err.toJson());
    }
  } else {
    ExceptionMapper.forEach(element => {
      if (err instanceof element) {
        return res.status(err.status_code).json(err.toJson());
      }
    });
    return res.status(500).send(err.stack);
  }
});

if (!isProduction) {
  app._router.stack.forEach(function(r) {
    if (r.route && r.route.path) {
      console.log(r.route.path);
    }
  });
}

module.exports = app;
