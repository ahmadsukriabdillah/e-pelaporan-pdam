const faker = require("faker");
var db = require("knex")({
  client: "pg",
  version: "7.10",
  connection: {
    host: "127.0.0.1",
    port: 5432,
    database: "e-pelaporan",
    user: "postgres",
    password: "1234"
  },
  pool: {
    min: 0,
    max: 10
  },
  debug: false
});
let nomor = 1;
let nomor_log = 1;
const getRandomizer = (bottom, top) => {
  return Math.floor(Math.random() * (1 + top - bottom)) + bottom;
};
const gen_ticket_id = () => {
  nomor++;
  return "C1904." + nomor.toString().padStart(5, "0");
};
const nomor_laporan_array = [
  42,
  45,
  48,
  51,
  54,
  57,
  60,
  63,
  66,
  69,
  72,
  75,
  83,
  84,
  85
];

const matrix_array = [2658, 2572, 2573, 2574, 2571];
const status = [
  "Menunggu",
  "Eskalasi",
  "Teruskan",
  "Tertunda",
  "Proses",
  "Ditangani",
  "Selesai"
];
const call_id = ["Call", "APP"];
const ocr = ["OCR", "N-OCR"];
const create = () => {
  return {
    id: nomor,
    ticket_id: gen_ticket_id(),
    nomor_laporan: nomor_laporan_array[getRandomizer(0, 14)],
    ticket_date: faker.date.recent(),
    call_id: faker.phone.phoneNumber(),
    ticket_src: call_id[getRandomizer(0, 1)],
    contact_name: faker.name.findName(),
    address: faker.address.streetAddress(),
    email: faker.internet.email(),
    mobile_phone: faker.phone.phoneNumber(),
    matrix_id: matrix_array[getRandomizer(0, 4)],
    remark: faker.lorem.sentence(),
    action: ocr[getRandomizer(0, 1)],
    status: status[getRandomizer(0, 6)],
    upd: "Customer",
    tenant_id: "SC_PDAM"
  };
};
async function main() {
  await db("contact").truncate();
  await db("contact_log").truncate();

  for (let i = 0; i < 9000; i++) {
    let data = await db("contact")
      .insert(create())
      .returning([
        "ticket_id",
        "remark as feedback",
        "upd",
        "status",
        "tenant_id"
      ]);
    for (let n = 0; n < getRandomizer(3, 7); n++) {
      let data_log = {
        ...data[0],
        feedback: faker.lorem.sentence(),
        id: nomor_log,
        lup: faker.date.future()
      };
      let contac = await db("contact_log").insert(data_log);
      nomor_log++;
    }
  }
  console.log("done");
}

main()
  .then()
  .catch(err => console.error(err));
